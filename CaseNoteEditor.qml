import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0
import dental.clear 1.0

Page {
    id: editCaseNotePage
    title: qsTr("Edit Case note")
    property var editText;

    TextEdit {
        id: editMe
        focus:true
        text: editText
        anchors.centerIn: parent
        width: parent.width /2
        height: parent.height/2
    }

    footer: ToolBar {
        RowLayout {
            anchors.fill: parent

            Button {
                text: "Copy"
                Layout.alignment: Qt.AlignHCenter
                onClicked: {editMe.selectAll();editMe.copy();}

            }

            Button {
                text: "Print"
                CaseNotePrinter {
                    id: printer
                }
                onClicked: printer.print5264Note(editMe.text,spotIndex.value);
            }
            Label {
                text: "Item Index:"
            }
            SpinBox {
                id: spotIndex
                from: 1
                to: 6

            }

            Button {
                text: "Exit"
                Layout.alignment: Qt.AlignHCenter
                onClicked: Qt.quit();
            }

        }


    }

}
