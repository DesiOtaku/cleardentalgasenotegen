import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0

RowLayout {

    property var matType: getMatrixType()
    property bool isPost: true

    ButtonGroup {
        id: buttGroup
        buttons: [noMatrix,tofflemire,compositight]
    }


    function getMatrixType() {
        if(noMatrix.checked) {
            return "None";
        }

        if(isPost) {
            if(tofflemire.checked) {
                return  "Tofflemire";
            }
            if(compositight.checked) {
                return  "Composi-Tight";
            }
        } else {
            return "Mylar"
        }
    }

    RadioButton {
        id: noMatrix
        text: "None"
        checked: true
    }

    StackView {
        id: stView
        width: theBigRow.width
        height: theBigRow.height

        property bool isPostSelect: true;

        initialItem: Row {
            id: theBigRow
            RadioButton {
                id: tofflemire
                text: "Tofflemire"
            }
            RadioButton {
                id: compositight
                text: "Composi-Tight"
            }
        }

        Component {
            id: antPage
            RadioButton {
                id: mylar
                text: "Mylar"
            }
        }



        function updateViewForTooth(newIsPot) {
            if(newIsPot && (!isPostSelect)) {
                pop();
                isPostSelect = true;

            }  else if((!newIsPot) && isPostSelect) {
                push(antPage);
                isPostSelect = false;
                buttGroup.buttons.push(stView.currentItem);
            }
            noMatrix.checked = true;
        }




        pushEnter: Transition {
            ParallelAnimation {
                PropertyAnimation {
                    property: "opacity"
                    from: 0
                    to:1
                    duration: 200
                }
                PropertyAnimation {
                    property: "x"
                    from: firstItem.width
                    to:0
                    duration: 200
                }
            }
        }
        pushExit: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 1
                to:0
                duration: 200
            }
        }
        popEnter: Transition {
            ParallelAnimation {
                PropertyAnimation {
                    property: "opacity"
                    from: 0
                    to:1
                    duration: 200
                }
                PropertyAnimation {
                    property: "x"
                    from: firstItem.width
                    to:0
                    duration: 200
                }
            }
        }
        popExit: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 1
                to:0
                duration: 200
            }
        }
    }

    onIsPostChanged: stView.updateViewForTooth(isPost)


}
