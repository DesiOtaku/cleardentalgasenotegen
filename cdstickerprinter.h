#ifndef CDSTICKERPRINTER_H
#define CDSTICKERPRINTER_H

#include <QObject>

class CDStickerPrinter : public QObject
{
    Q_OBJECT
public:
    explicit CDStickerPrinter(QObject *parent = nullptr);

signals:

public slots:
    static void print5264Note(QString note, int index);
};

#endif // CDSTICKERPRINTER_H
