#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>

#include <QApplication>

#include "cdstickerprinter.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);


    QQmlApplicationEngine engine;

    qmlRegisterType<CDStickerPrinter>("dental.clear", 1, 0, "CaseNotePrinter");

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
