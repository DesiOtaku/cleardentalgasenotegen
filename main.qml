import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.1
import QtQuick.Layouts 1.0

ApplicationWindow {
    visible: true
    width: 1024
    height: 768

    header: ToolBar {
        RowLayout {
            anchors.fill: parent
            ToolButton {
                text: "‹"
                visible: theStack.depth > 1
                onClicked: theStack.pop();
            }

            Label {
                text: theStack.currentItem.title
                horizontalAlignment: Qt.AlignHCenter
                Layout.fillWidth: true
            }

        }

    }

    StackView {
        id: theStack
        anchors.fill: parent
        initialItem: SelectProcedurePage{}
    }
}
