#include "cdstickerprinter.h"

#include <QPrinter>
#include <QPageSize>
#include <QPaintEngine>
#include <QPainter>
#include <QDebug>
#include <QPrintDialog>


CDStickerPrinter::CDStickerPrinter(QObject *parent) : QObject(parent)
{

}


void CDStickerPrinter::print5264Note(QString note, int index) {
    QPrinter printer(QPrinter::HighResolution);
    QPrintDialog printDialog(&printer);
    if (printDialog.exec() != QDialog::Accepted) {
        printer.setPageSize(QPageSize(QPageSize::Letter));
        printer.setOutputFileName("test.pdf");
    }

    //QPrinter printer(QPrinter::HighResolution);

    printer.setPageMargins(QMarginsF(0,0,0,0));

    qDebug()<<printer.paperRect().width();
    qreal scaler = printer.paperRect().width() / 8.5;
    qDebug()<<scaler * .1;


    QPainter painter;
    painter.begin(&printer);

    QRectF whereToDraw(.12 * scaler,.5 * scaler, 4 * scaler,3.33 * scaler * 2);
    if(index==2) {
        whereToDraw.moveLeft(4.25 * scaler);
        qDebug()<<whereToDraw;
    }
    else if (index==3) {
        whereToDraw.moveTop(3.9 * scaler);
    }
    else if (index==4) {
        whereToDraw.moveLeft(4.25 * scaler);
        whereToDraw.moveTop(3.9 * scaler);
    }
    else if (index==5) {
        whereToDraw.moveTop(7.25 * scaler);
    }
    else if (index==6) {
        whereToDraw.moveLeft(4.25 * scaler);
        whereToDraw.moveTop(7.25 * scaler);
    }

    painter.setFont(QFont("Serif",8));

    painter.drawText(whereToDraw,note);

    painter.end();

}
