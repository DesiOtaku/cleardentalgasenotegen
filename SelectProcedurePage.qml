import QtQuick 2.9
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0

Page {
    title: qsTr("Select Procedure Type")

    Column {
        anchors.centerIn: parent
        spacing: opButton.height
        Button {
            id: opButton
            text: "Operative / Filling"
            font.pointSize: 16
            onClicked: theStack.push(opFirstPage);
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Button {
            text: "Crown"
            font.pointSize: 16
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Button {
            text: "Extraction"
            font.pointSize: 16
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Button {
            text: "Exam"
            font.pointSize: 16
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Button {
            text: "Denture"
            font.pointSize: 16
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Row {
            spacing: 10
            Label {
                text: "Who:"
                font.pointSize: 16
            }
            TextField {
                id: whoText
                placeholderText: "Who is doing the procedure"
            }
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }

    Component {
        id: opFirstPage
        OpFirstPage{}
    }
}
