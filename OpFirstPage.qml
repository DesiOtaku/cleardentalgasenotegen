import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10

Page {
    id: opPage
    title: qsTr("Operative")

    ScrollView {
        anchors.fill: parent
        clip: true
        contentWidth: parent.width

        GridLayout {
            columns: 2
            anchors.centerIn: parent


            ///Tooth number
            Label {
                text: "Tooth #: "
            }

            ToothNumbsList{
                id: toothList;
                onSelectedToothChanged: surfaceChoose.updateViewForTooth(selectedTooth)
            }

            MenuSeparator{Layout.columnSpan: 2; width: opPage.width}

            Label {
                text: "Surfaces:"
            }

            StackView {
                id: surfaceChoose

                property bool isPostSelect: true;
                width: firstItem.width
                height: firstItem.height

                initialItem: PostSurfaceChoose{id: firstItem}

                function updateViewForTooth(newToothNumb) {
                    if(isPosterior(newToothNumb) && (!isPostSelect)) {
                        pop();
                        isPostSelect = true;
                    }  else if((!isPosterior(newToothNumb)) && isPostSelect) {
                        push(antPage);
                        isPostSelect = false;
                    }
                }

                pushEnter: Transition {
                    ParallelAnimation {
                        PropertyAnimation {
                            property: "opacity"
                            from: 0
                            to:1
                            duration: 200
                        }
                        PropertyAnimation {
                            property: "x"
                            from: firstItem.width
                            to:0
                            duration: 200
                        }
                    }
                }
                pushExit: Transition {
                    PropertyAnimation {
                        property: "opacity"
                        from: 1
                        to:0
                        duration: 200
                    }
                }
                popEnter: Transition {
                    ParallelAnimation {
                        PropertyAnimation {
                            property: "opacity"
                            from: 0
                            to:1
                            duration: 200
                        }
                        PropertyAnimation {
                            property: "x"
                            from: firstItem.width
                            to:0
                            duration: 200
                        }
                    }
                }
                popExit: Transition {
                    PropertyAnimation {
                        property: "opacity"
                        from: 1
                        to:0
                        duration: 200
                    }
                }

                Component {
                    id: antPage
                    AntSurfaceChoose{}
                }

                function getTheSurfaces() {
                    return surfaceChoose.currentItem.choosenSurfaces;
                }
            }

            MenuSeparator{Layout.columnSpan: 2; width: opPage.width}

            Label {
                text: "Material:"
            }
            Row {
                CheckBox {
                    id:flowComp
                    text: "Flowable Composite"
                }
                CheckBox {
                    id:packComp
                    text: "Packable Composite"
                }
                CheckBox {
                    id: amalgamUsed
                    text: "Amalgam"
                }
            }

            MenuSeparator{Layout.columnSpan: 2}


            Label {
                text: "Medical History:"
            }

            Row {
                RadioButton {
                    text: "No changes"
                    checked: true
                }
                RadioButton {
                    id: medChanges
                    text: "Reviewed changes"
                }
            }

            MenuSeparator{Layout.columnSpan: 2}


            ///Local An
            Label {
                text: "4% Septocaine with 1:100,000 epi."
            }
            SpinBox {
                id: septoCount
                from: 0
                to: 6
                stepSize: 1
            }

            Label {
                text: "2% Lidocaine with 1:100,000 epi."
            }
            SpinBox {
                id: lidoCount
                from: 0
                to: 6
                stepSize: 1
            }

            Label {
                text: "3% Carbocaine"
            }
            SpinBox {
                id: carboCount
                from: 0
                to: 6
                stepSize: 1
            }

            MenuSeparator{Layout.columnSpan: 2}

            Label {
                text: "Liner:"
            }

            Row {
                RadioButton {
                    text: "None"
                    checked: true
                }
                RadioButton {
                    id: limelight
                    text: "Lime-Lite™"
                }
            }

            MenuSeparator{Layout.columnSpan: 2}

            Label {
                text: "Matrix:"
            }

            MatrixType{id: matSelected; isPost: isPosterior(toothList.selectedTooth) }

            Label {
                text: "Wedge:"
                opacity: matSelected.matType !== "None"
                Behavior on opacity {
                    NumberAnimation { duration: 500 }
                }
            }

            Row {
                RadioButton {
                    id: placedWedge
                    text: "Yes"
                }
                RadioButton {
                    text: "No"
                }
                opacity: matSelected.matType !== "None"
                Behavior on opacity {
                    NumberAnimation { duration: 500 }
                }
            }

            MenuSeparator{Layout.columnSpan: 2}

            Label {
                text: "Next Visit:"
            }
            TextField {
                id: nextVisit
                placeholderText: "What should the next visit be?"
            }
        }


    }



    footer: Button {
        text: "Done"
        onClicked:  {
            theStack.push(caseEditor, {"editText": makeTheText()})
        }
    }

    Component {
        id: caseEditor
        CaseNoteEditor {}
    }


    function makeTheText() {
        var returnMe = "Patient presented for tooth #" + toothList.selectedTooth;

        var surfaces = surfaceChoose.getTheSurfaces();
        returnMe +=" " + surfaces;

        if(amalgamUsed.checked) {
            returnMe += " amalgam.\n";
        } else {
            returnMe += " composite.\n";
        }

        if(medChanges.checked) {
            returnMe += "Reviewed changes in medical history.\n";
        } else {
            returnMe += "No changes in medical history.\n";
        }

        if(septoCount.value > 0) {
            returnMe += "Administered " + septoCount.value + " carpule(s) (" +
                    (septoCount.value * 1.7) + " ml) of "+
                    "4% Septocaine with 1:100,000 epinephrine via infiltration.\n";
        }

        if(carboCount.value > 0) {
            returnMe += "Administered " + carboCount.value + " carpule(s) (" +
                    (carboCount.value * 1.7) + " ml) of "+
                    "3% Carbocaine via infiltration.\n";
        }

        if(lidoCount.value > 0) {
            returnMe += "Administered " + lidoCount.value + " carpule(s) (" +
                    (lidoCount.value * 1.7) + " ml) of "+
                    "2% Lidocaine with 1:100,000 epinephrine via "+
                    "inferior alveolar nerve block.\n";
        }

        returnMe += "Removed caries using a high speed and low speed handpiece.\n";
        //returnMe += "Placed chlorhexidine gluconate and air dried.\n";

        if(amalgamUsed.checked) {
            returnMe += "Copalite placed and dried.\n";
        }

        if(limelight.checked) {
            returnMe += "Placed Lime-Lite.\n";
        }

        if(matSelected.getMatrixType()  === "Tofflemire") {
            if(placedWedge.checked) {
                returnMe += "Placed a Tofflemire matrix and band along with a wodden wedge.\n";
            } else {
                returnMe += "Placed a Tofflemire matrix and band.\n";
            }
        }

        if(matSelected.getMatrixType()  === "Composi-Tight") {
            if(placedWedge.checked) {
                returnMe += "Placed a Composi-Tight band along with a wodden wedge.\n";
            } else {
                returnMe += "Placed a Composi-Tight band.\n";
            }
        }

        else if(matSelected.getMatrixType() === "Mylar") {
            if(placedWedge.checked) {
                returnMe += "Placed a Mylar band along with a wodden wedge.\n";
            } else {
                returnMe += "Placed a Mylar band.\n";
            }
        }

        if(flowComp.checked) {
            returnMe += "Etch placed and rinsed. Bond placed and light cured.\n";
            returnMe += "Placed flowable composite in layers and light cured.\n";
        }

        if(packComp.checked) {
            returnMe += "Etch placed and rinsed. Bond placed and light cured.\n";
            returnMe += "Placed packable composite in layers and light cured.\n";
        }

        if(amalgamUsed.checked) {
            returnMe += "Amalgam placed and condensed.\n";
        }

        returnMe += "Excess removed.\n"
        returnMe += "Occlusion adjusted.\n"

        if((surfaces.indexOf("M") > 0) || (surfaces.indexOf("D") > 0)) {
            returnMe += "Interproxmial contact confirmed via floss.\n"
        }

        returnMe += "Patient tolerated the procedure well.\n"
        returnMe += "Next Visit: " + nextVisit.text;

        return returnMe;
    }

    function isPosterior(getToothNumb) {
        var numbForm = Number(getToothNumb);
        var returnMe = true;

        if((numbForm >= 6) && (numbForm <= 11)) { //maxillary anterior
            returnMe = false;
        } else if((numbForm >= 22) && (numbForm <= 27)) { //mandibular anterior
            returnMe = false;
        }

        return returnMe;
    }
}
