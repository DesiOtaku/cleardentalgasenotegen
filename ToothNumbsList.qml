import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0

Column {
    property var selectedTooth: theButtonGroup.checkedButton.text
    ButtonGroup {
        id:theButtonGroup
    }

    Row {
        id: row1
        RoundButton {
            text: "1"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            checked: true
            onClicked: checked = true
        }
        RoundButton {
            text: "2"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "3"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "4"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "5"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "6"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "7"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "8"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "9"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "10"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "11"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "12"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "13"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "14"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "15"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "16"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
    }


    Row {
        id: row2
        RoundButton {
            text: "32"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "31"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "30"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "29"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "28"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "27"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "26"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "25"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "24"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "23"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "22"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "21"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "20"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "19"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "18"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
        RoundButton {
            text: "17"
            width: font.pixelSize  *3
            ButtonGroup.group: theButtonGroup
            onClicked: checked = true
        }
    }


}
